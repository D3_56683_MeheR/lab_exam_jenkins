const { query } = require('express')

const express = require ('express')

const router = express.Router()

const utils = require('../utils')

const db = require('../db')




//GET  --> Display Movie details using name from Containerized MySQL
router.get('/:movie_title', (request, response) => {
    const{movie_title} = request.params

    const statement = `SELECT * FROM movie where movie_title = '${movie_title}'`

    db.execute(statement, (error, result) => {
            response.send(utils.createResult(error, result))
    })
})




//POST --> ADD Movie data into Containerized MySQL table
router.post('/add', (request, response) => {
    const { movie_title, movie_release_date, movie_time, director_name } = request.body
  
    const statement = `
          insert into movie
            (movie_title, movie_release_date, movie_time, director_name)
          values
            ( '${movie_title}', '${movie_release_date}', ${movie_time}, '${director_name}')
        `
    db.execute(statement, (error, result) => {
      response.send(utils.createResult(error, result));
    })
  })




//UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table
router.put('/update/:movie_title', (request, response) => {
    const {movie_title} = request.params
    const {movie_release_date, movie_time } = request.body

    const statement = `UPDATE movie SET movie_release_date = '${movie_release_date}' AND movie_time = ${movie_time} where movie_title = '${movie_title}'`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})





//DELETE --> Delete Movie from Containerized MySQL
router.delete('/delete/:movie_id', (request, response) => {
    const{movie_id} = request.params
    const statement = `DELETE FROM movie WHERE movie_id = ${movie_id}`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router;
