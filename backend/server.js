const express = require('express')

const app = express()

app.use(express.json())

const cors = require("cors");
app.use(cors("*"));


const routerMovie = require('./routes/movie')

app.use('/movie',routerMovie)


app.listen(4000, () =>{
    console.log('Server Started on port 4000')
})

