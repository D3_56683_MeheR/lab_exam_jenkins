CREATE TABLE movie (
    movie_id INTEGER PRIMARY KEY auto_increment,
    movie_title VARCHAR (50),
    movie_release_date VARCHAR (20),
    movie_time FLOAT,
    director_name VARCHAR (30)
);
